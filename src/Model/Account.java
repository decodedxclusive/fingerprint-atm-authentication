/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.AccountHolder;
import POJO.ResetUserPin;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author the Code
 */
public class Account {

    private Connection con;
    private Statement stmt;
    private PreparedStatement ps;
    private ResultSet result;
    Date today = new Date();
    long time = today.getTime();
    Timestamp timeStamp = new Timestamp(time);

    /*
    *add new card associated with an account number
    *@param AccountHolder account
    *@param byte[] rightThumb
    *@param byte[] rightIndex
    *@return status (account and card details of customer)
     */
    public Map NewAccount(AccountHolder account, byte[] rightThumb, byte[] rightIndex) throws SQLException {
        Map<String, String> status = new HashMap<>();
        try {
            String accountNo = generateAccountNumber();
            String cardNo = generateCardNumber();
            String pincode = generatePin();

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number already exists
            result = stmt.executeQuery("SELECT * FROM user WHERE card_number = " + cardNo);
            //if yes generate another one
            if (result.next()) {
                NewAccount(account, rightThumb, rightIndex);
            } else {
                con.setAutoCommit(false);
                //if no, insert the account details associated with the card
                String sql = "INSERT INTO user (first_name, last_name, account_number, card_number, card_type, account_type, account_balance, active, creation_date, alt_user) VALUES(?,?,?,?,?,?,?,?,?,?)";
                ps = con.prepareStatement(sql);

                ps.setString(1, account.getFirstName());
                ps.setString(2, account.getLastName());
                ps.setString(3, accountNo);
                ps.setString(4, cardNo);
                ps.setString(5, account.getCardType());
                ps.setString(6, account.getAccountType());
                ps.setDouble(7, 0.0);
                ps.setInt(8, 1);
                ps.setTimestamp(9, timeStamp);
                ps.setInt(10, 0);

                ps.execute();

                //Insert card details and pin
                String sql2 = "INSERT INTO card (card_number, pin, account_number, f1, f2, creation_date) VALUES(?,?,?,?,?,?)";
                ps = con.prepareStatement(sql2);

                ps.setString(1, cardNo);
                ps.setString(2, pincode);
                ps.setString(3, accountNo);
                ps.setBytes(4, rightThumb);
                ps.setBytes(5, rightIndex);
                ps.setTimestamp(6, timeStamp);

                ps.execute();

                //save transaction once all insert has been done.
                con.commit();

                //Send details of account and card
                status.put("Error", "false");
                status.put("AccountNo", accountNo);
                status.put("CardNo", cardNo);
                status.put("Pin", pincode);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            //rollback to previous state of db before transaction
            con.rollback();
            System.out.println(ex.getMessage());
            status.put("Error", "true");
        } finally {
            //close all connections
            ps.close();
            con.close();
        }
        return status;
    }

    /*
    *add new user that has access to this card
    *@param AccountHolder account
    *@param byte[] rightThumb
    *@param byte[] rightIndex
    *@return status (account and card details of customer)
     */
    public Map AlternateUser(AccountHolder account, byte[] rightThumb, byte[] rightIndex) throws SQLException {
        Map<String, String> status = new HashMap<>();
        try {

            String pincode = generatePin();

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");

            con.setAutoCommit(false);
            //if no, insert the account details associated with the card
            String sql = "INSERT INTO alternate_user (first_name, last_name, account_number, creation_date) VALUES(?,?,?,?)";
            ps = con.prepareStatement(sql);

            ps.setString(1, account.getFirstName());
            ps.setString(2, account.getLastName());
            ps.setString(3, account.getAccountNumber());
            ps.setTimestamp(4, timeStamp);

            ps.execute();

            //Insert card details and pin
            String sql2 = "UPDATE user SET alt_user = ? WHERE account_number = ?";
            ps = con.prepareStatement(sql2);

            ps.setInt(1, 1);
            ps.setString(2, account.getAccountNumber());

            ps.execute();

            //Insert card details and pin
            String sql3 = "UPDATE card SET pin2 = ?, a1 = ?, a2 = ? WHERE account_number = ?";
            ps = con.prepareStatement(sql3);

            ps.setString(1, pincode);
            ps.setBytes(2, rightThumb);
            ps.setBytes(3, rightIndex);
            ps.setString(4, account.getAccountNumber());

            ps.execute();

            //save transaction once all insert has been done.
            con.commit();

            //Send details of account and card
            status.put("Error", "false");
            status.put("Pin", pincode);

        } catch (ClassNotFoundException | SQLException ex) {
            //rollback to previous state of db before transaction
            con.rollback();
            System.out.println(ex.getMessage());
            status.put("Error", "true");
        } finally {
            //close all connections
            ps.close();
            con.close();
        }
        return status;
    }

    /*
    *update the user password
    *@param AccountHolder account
    *@return status
     */
    public Map UpdateUserPassword(ResetUserPin user) throws SQLException {
        Map<String, String> status = new HashMap<>();

        if (String.valueOf(user.getConfirmPin()).equals(String.valueOf(user.getNewPin()))) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");

                stmt = con.createStatement();
                //check if card number exists
                result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + user.getCardNumber());
                //if yes, check if password matches
                if (result.next()) {
                    if (String.valueOf(user.getOldPin()).equals(result.getString("pin"))) {

                        con.setAutoCommit(false);
                        //if no, insert the account details associated with the card
                        String sql = "UPDATE card SET pin = ? WHERE card_number = ?";
                        ps = con.prepareStatement(sql);

                        ps.setString(1, String.valueOf(user.getConfirmPin()));
                        ps.setString(2, user.getCardNumber());

                        ps.execute();

                        ps.close();
                        //save transaction once all insert has been done.
                        con.commit();

                        //Send details of account and card
                        status.put("Error", "false");

                    } else {
                        status.put("Error", "true");
                        status.put("message", "Wrong Old Pin");
                    }
                } else {
                    status.put("Error", "true");
                    status.put("message", "Invalid card number");
                }

            } catch (ClassNotFoundException | SQLException ex) {
                //rollback to previous state of db before transaction
                con.rollback();
                status.put("Error", "true");
            } finally {
                //close all connections
                con.close();
            }
        } else {
            status.put("Error", "true");
            status.put("message", "Please confirm pin");
        }

        return status;
    }

    /*
    *update the alternate user password
    *@param AccountHolder account
    *@return status
     */
    public Map UpdateAlternateUserPassword(ResetUserPin user) throws SQLException {
        Map<String, String> status = new HashMap<>();

        if (String.valueOf(user.getConfirmPin()).equals(String.valueOf(user.getNewPin()))) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");

                stmt = con.createStatement();
                //check if card number exists
                result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + user.getCardNumber());
                //if yes, check if password matches
                if (result.next()) {
                    if (String.valueOf(user.getOldPin()).equals(result.getString("pin2"))) {

                        con.setAutoCommit(false);
                        //if no, insert the account details associated with the card
                        String sql = "UPDATE card SET pin2 = ? WHERE card_number = ?";
                        ps = con.prepareStatement(sql);

                        ps.setString(1, String.valueOf(user.getConfirmPin()));
                        ps.setString(2, user.getCardNumber());

                        ps.execute();

                        ps.close();

                        //save transaction once all insert has been done.
                        con.commit();

                        //Send details of account and card
                        status.put("Error", "false");

                    } else {
                        status.put("Error", "true");
                        status.put("message", "Wrong Old Pin");
                    }
                } else {
                    status.put("Error", "true");
                    status.put("message", "Invalid card number");
                }

            } catch (ClassNotFoundException | SQLException ex) {
                //rollback to previous state of db before transaction
                con.rollback();
                status.put("Error", "true");
            } finally {
                //close all connections
                con.close();
            }
        } else {
            status.put("Error", "true");
            status.put("message", "Please confirm pin");
        }
        return status;
    }

    /*
    *generate new 10 digits account number
    *@return accountNo
     */
    public String generateAccountNumber() {
        Random rand = new Random();
        String accountNo = "27";
        char[] numbers = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        for (int i = 0; i < 8; i++) {
            int num = rand.nextInt(9) + 0;
            accountNo = accountNo + numbers[num];
        }
        return accountNo;
    }

    /*
    *generate new 16 digits card number
    *@return cardNo
     */
    public String generateCardNumber() {
        Random rand = new Random();
        String cardNo = "5";
        char[] numbers = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
        for (int i = 0; i < 15; i++) {
            int num = rand.nextInt(9) + 0;
            cardNo = cardNo + numbers[num];
        }
        return cardNo;
    }

    /*
    *generate 4 digits pin
    *@return pin
     */
    public String generatePin() {
        Random rand = new Random();
        int num = rand.nextInt(8999) + 1000;
        return Integer.toString(num);
    }

}
