/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author the Code
 */
public class ResetUserPin {
    private final String cardNumber;
    private char[] oldPin;
    private char[] newPin;
    private char[] confirmNewPin;
    private final Boolean alternateUser;
    
    public ResetUserPin(String cardNumber, Boolean alternateUser){
        this.cardNumber = cardNumber;
        this.alternateUser = alternateUser;
    }
    
    public String getCardNumber(){
        return cardNumber;
    }
    
    public Boolean isAlternateUser(){
        return alternateUser;
    }
    
    public char[] getOldPin(){
        return oldPin;
    }
    
    public void setOldPin(char[] pin){
        this.oldPin = pin;
    }
    
    public char[] getNewPin(){
        return newPin;
    }
    
    public void setNewPin(char[] pin){
        this.newPin = pin;
    }
    
    public char[] getConfirmPin(){
        return confirmNewPin;
    }
    
    public void setConfirmPin(char[] pin){
        this.confirmNewPin = pin;
    }
}
