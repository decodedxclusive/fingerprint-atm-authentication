/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTextArea;

/**
 *
 * @author the Code
 */
public class Login {

    private Connection con;
    private Statement stmt;
    private PreparedStatement ps;
    private ResultSet result;
    Date today = new Date();
    long time = today.getTime();
    Timestamp timeStamp = new Timestamp(time);

    /*
    *authenticate the card number to check if its in the db
    *and return customer's names.
    *@return status
     */
    public Map validateCard(String cardNo) throws SQLException {
        Map<String, String> status = new HashMap<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number exists in db
            result = stmt.executeQuery("SELECT * FROM user WHERE card_number = " + cardNo);
            //if yes, return the first and last name and card number of customer 
            if (result.next()) {
                status.put("Error", "false");
                status.put("Card", result.getString("card_number"));
                status.put("Name", result.getString("first_name") + " " + result.getString("last_name"));
            } else {
                status.put("Error", "true");
                status.put("Message", "Invalid Card Number");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            status.put("Error", "true");
            status.put("Message", "An error occured during validation");
        } finally {
            //close all connections
            stmt.close();
            con.close();
        }
        return status;
    }

    /*
    *validate pin to check if its the right pin for the card
    *@return status
     */
    public Map validatePin(String cardNo, char[] pin) throws SQLException {
        Map<String, String> status = new HashMap<>();
        String text = String.valueOf(pin);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number exists
            result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + cardNo);
            //if yes, check if password matches
            if (result.next()) {
                if (text.equals(result.getString("pin"))) {
                    status.put("Error", "false");
                    status.put("Card", cardNo);
                    status.put("Account", getAccountNumber(cardNo));
                    status.put("Alternate", "false");
                } else {
                    result = stmt.executeQuery("SELECT * FROM user WHERE card_number = " + cardNo + " AND alt_user = '1'");
                    //if yes, check if password matches
                    if (result.next()) {
                        status = validateAlternatePin(cardNo, pin);
                    } else {
                        status.put("Error", "true");
                        status.put("Message", "Incorrect Pin");
                    }
                }
            } else {
                status.put("Error", "true");
                status.put("Message", "Invalid Card Number");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            status.put("Error", "true");
            status.put("Message", "An error occured during validation");
        } finally {
            //close all connections
            stmt.close();
            con.close();
        }
        return status;
    }

    /*
    *validate alternate pin to check if its the second user pin for the card
    *@return status
     */
    public Map validateAlternatePin(String cardNo, char[] pin) throws SQLException {
        Map<String, String> status = new HashMap<>();
        String text = String.valueOf(pin);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number exists
            result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + cardNo);
            //if yes, check if password matches
            if (result.next()) {
                if (text.equals(result.getString("pin2"))) {
                    status.put("Error", "false");
                    status.put("Card", cardNo);
                    status.put("Account", getAccountNumber(cardNo));
                    status.put("Alternate", "true");
                } else {
                    status.put("Error", "true");
                    status.put("Message", "Incorrect Pin");
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            status.put("Error", "true");
            status.put("Message", "An error occured during validation");
        } finally {
            //close all connections
            stmt.close();
            con.close();
        }
        return status;
    }

    /*
    *validate fingerprint of user( right thumb and right index finger )
    *@return status
     */
    public Map validateFingerPrint(String cardNo, DPFPSample rightThumb, DPFPSample rightIndex, JTextArea statusLabel) throws SQLException {
        Map<String, String> status = new HashMap<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number exists
            result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + cardNo);
            //if yes, check if fingerprint matches
            if (result.next()) {
                byte[] a = result.getBytes("f1");
                byte[] b = result.getBytes("f2");

                DPFPTemplate temp1 = DPFPGlobal.getTemplateFactory().createTemplate();
                temp1.deserialize(a);

                DPFPTemplate temp2 = DPFPGlobal.getTemplateFactory().createTemplate();
                temp2.deserialize(b);

                FingerprintVerification dp = new FingerprintVerification(statusLabel);
                if (dp.verify(null, rightThumb, temp1) && dp.verify(null, rightIndex, temp2)) {
                    status.put("Error", "false");
                    status.put("Card", cardNo);
                    status.put("Account", getAccountNumber(cardNo));
                    status.put("Alternate", "false");

                    //last login for alternate user
                    String sql2 = "UPDATE user SET last_login = ? WHERE account_number = ?";
                    ps = con.prepareStatement(sql2);

                    ps.setTimestamp(1, timeStamp);
                    ps.setString(2, getAccountNumber(cardNo));

                    ps.execute();

                } else {
                    result = stmt.executeQuery("SELECT * FROM user WHERE card_number = " + cardNo + " AND alt_user = '1'");
                    //if yes, check if password matches
                    if (result.next()) {
                        status = validateAlternateFingerPrint(cardNo, rightThumb, rightIndex, statusLabel);
                    } else {
                        status.put("Error", "true");
                        status.put("Message", "Incorrect fingerprint");
                    }

                }
            } else {
                status.put("Error", "true");
                status.put("Message", "Incorrect card");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            status.put("Error", "true");
            status.put("Message", "An error occured during validation");
        } finally {
            //close all connections
            stmt.close();
            con.close();
        }
        return status;
    }

    /*
    *validate fingerprint of alternate user( right thumb and right index finger )
    *@return status
     */
    public Map validateAlternateFingerPrint(String cardNo, DPFPSample rightThumb, DPFPSample rightIndex, JTextArea statusLabel) throws SQLException {
        Map<String, String> status = new HashMap<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number exists
            result = stmt.executeQuery("SELECT * FROM card WHERE card_number = " + cardNo);
            //if yes, check if fingerprint matches
            if (result.next()) {
                byte[] a = result.getBytes("a1");
                byte[] b = result.getBytes("a2");

                DPFPTemplate temp1 = DPFPGlobal.getTemplateFactory().createTemplate();
                temp1.deserialize(a);

                DPFPTemplate temp2 = DPFPGlobal.getTemplateFactory().createTemplate();
                temp2.deserialize(b);

                FingerprintVerification dp = new FingerprintVerification(statusLabel);
                if (dp.verify(null, rightThumb, temp1) && dp.verify(null, rightIndex, temp2)) {
                    status.put("Error", "false");
                    status.put("Card", cardNo);
                    status.put("Account", getAccountNumber(cardNo));
                    status.put("Alternate", "true");

                    //last login for alternate user
                    String sql2 = "UPDATE user SET last_login = ? WHERE account_number = ?";
                    ps = con.prepareStatement(sql2);

                    ps.setTimestamp(1, timeStamp);
                    ps.setString(2, getAccountNumber(cardNo));

                    ps.execute();

                } else {
                    status.put("Error", "true");
                    status.put("Message", "Incorrect fingerprint");
                }
            } else {
                status.put("Error", "true");
                status.put("Message", "Incorrect card");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            status.put("Error", "true");
            status.put("Message", "An error occured during validation");
        } finally {
            //close all connections
            stmt.close();
            con.close();
        }
        return status;
    }
    
    public String getAccountNumber(String cardNo) {
        String name = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/atm", "root", "");
            stmt = con.createStatement();
            //check if card number already exists
            result = stmt.executeQuery("SELECT account_number FROM user WHERE card_number = " + cardNo);
            //if yes generate another one
            if (result.next()) {
                name = result.getString("account_number");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return name;
    }

}
