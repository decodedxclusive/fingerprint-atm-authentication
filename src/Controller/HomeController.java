/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import View.RegisterAlternateUser;
import View.CardNumber;
import View.Home;
import View.ResetPin;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author the Code
 */
public class HomeController {

    private final Home home;

    public HomeController(Home home) {
        this.home = home;
        
        this.home.setTitle("Home");
        
        this.home.getAddAltUser().addMouseListener(new AddAltUser());
        this.home.getResetPin().addMouseListener(new Reset());
        this.home.getLogout().addMouseListener(new Logout());
    }

    class AddAltUser implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            RegisterAlternateUser user = new RegisterAlternateUser();
            user.getAccountNumber().setText(home.getAccountNumber().getText());
            user.getCardNumber().setText(home.getCardNumber().getText());
            home.setVisible(false);
            user.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

    }
    
    class Reset implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            ResetPin reset = new ResetPin();
            reset.getCardNumber().setText(home.getCardNumber().getText());
            reset.setAccountNumber(home.getAccountNumber().getText());
            reset.setAlternateUser(home.isAlternateUser());
            home.setVisible(false);
            reset.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

    }

    class Logout implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            CardNumber user = new CardNumber();
            home.setVisible(false);
            user.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

    }
}
