/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Login;
import View.Fingerprint;
import View.Pin;
import View.CardNumber;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;

/**
 *
 * @author the Code
 */
public class PinController {

    public final Pin home;
    public final Login model;

    public PinController(Pin home, Login model) {
        this.home = home;
        this.model = model;
        
        this.home.setTitle("Pin Authentication");

        this.home.getBackHome().addMouseListener(new Back());
        this.home.getATMPin().addKeyListener(new CheckValues());
        this.home.getATMPin().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.home.getATMPin().getActionMap().put("pressed", new ValidatePinEnter());
        this.home.getATMLoginBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.home.getATMLoginBtn().getActionMap().put("pressed", new ValidatePinEnter());

        this.home.getATMLoginBtn().addActionListener(new ValidatePin());
    }

    public boolean Validate(JPasswordField pin, JLabel warning) {
        if (pin.getPassword().equals("")) {
            warning.setText("Please fill in pin");
            warning.setForeground(Color.red);
            pin.requestFocusInWindow();
            return true;
        }
        return false;
    }

    class ValidatePin implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (!Validate(home.getATMPin(), home.getWarning())) {
                Map response = new HashMap<>();
                try {
                    response = model.validatePin(home.getCardNo().getText(), home.getATMPin().getPassword());
                } catch (SQLException ex) {
                }
                if (((String) response.get("Error")).equals("false")) {
                    Fingerprint fg = new Fingerprint();
                    fg.getCardNumber().setText(((String) response.get("Card")));
                    home.setVisible(false);
                    fg.setVisible(true);
                } else if (((String) response.get("Error")).equals("true")) {
                    home.getWarning().setText((String) response.get("Message"));
                    home.getWarning().setForeground(Color.red);
                    home.getATMPin().requestFocusInWindow();
                }
            }
        }

    }

    class ValidatePinEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!Validate(home.getATMPin(), home.getWarning())) {
                Map response = new HashMap<>();
                try {
                    response = model.validatePin(home.getCardNo().getText(), home.getATMPin().getPassword());
                } catch (SQLException ex) {
                }
                if (((String) response.get("Error")).equals("false")) {
                    Fingerprint fg = new Fingerprint();
                    fg.getCardNumber().setText(((String) response.get("Card")));
                    home.setVisible(false);
                    fg.setVisible(true);
                } else if (((String) response.get("Error")).equals("true")) {
                    home.getWarning().setText((String) response.get("Message"));
                    home.getWarning().setForeground(Color.red);
                    home.getATMPin().requestFocusInWindow();
                }
            }
        }

    }

    //check To avoid illegal characters in textfields
    class CheckValues implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            char[] pass = home.getATMPin().getPassword();
            if (pass.length < 4) {
                if ((!(Character.isDigit(c))) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)) {
                    Toolkit.getDefaultToolkit().beep();
                    e.consume();
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    class Back implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            CardNumber user = new CardNumber();
            home.setVisible(false);
            user.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }
}
