/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author the Code
 */

public class AccountHolder {
    
    private String firstName;
    private String lastName;
    private String accountNumber;
    private String cardNumber;
    private String cardType;
    private String accountType;
    private Long balance;

    public AccountHolder(){
        
    }
    
    public AccountHolder(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public String getCardNumber() {
        return this.accountNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getAccountType(){
        return this.accountType;
    }
    
    public void setAccountType(String accountType){
        this.accountType = accountType;
    }
    
    public String getCardType(){
        return this.cardType;
    }
    
    public void setCardType(String cardType){
        this.cardType = cardType;
    }
    
    public Long getBalance(){
        return this.balance;
    }
    
    public void setBalance(Long balance){
        this.balance = balance;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

}
