/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.FingerprintVerification;
import Model.Login;
import View.CardNumber;
import View.Fingerprint;
import View.Home;
import com.digitalpersona.onetouch.DPFPSample;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;
import static javax.swing.text.DefaultCaret.ALWAYS_UPDATE;

/**
 *
 * @author the Code
 */
public class FingerprintController {

    public final Fingerprint print;
    FingerprintVerification dp;

    public FingerprintController(Fingerprint print) {
        this.print = print;

        this.print.setTitle("Fingerprint Authentication");

        DefaultCaret caret = (DefaultCaret) this.print.getStatus().getCaret();
        caret.setUpdatePolicy(ALWAYS_UPDATE);
        this.print.getStatus().setCaret(caret);
        this.print.getBackHome().addMouseListener(new Back());

        SwingWorker worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                FingerprintVerify fingerprint = new FingerprintVerify();
                return null;
            }
        };
        worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if ("DONE".equals(evt.getNewValue().toString())) {
            }
        });
        worker.execute();
    }

    class FingerprintVerify {

        public FingerprintVerify() {

            dp = new FingerprintVerification(print.getStatus());
            dp.listReaders();

            print.getError().setForeground(Color.GREEN);
            print.getError().setText("Scan your right thumb");

            DPFPSample fingerPrint1 = null;
            try {
                fingerPrint1 = dp.getSample(null, "Scan your right thumb\n");
                dp.process(print.getRightThumb(), fingerPrint1);
            } catch (InterruptedException ex) {
            }
            if (fingerPrint1 == null) {
                try {
                    throw new Exception();
                } catch (Exception ex) {
                }
            }

            print.getError().setText("Scan your right index finger");

            DPFPSample fingerPrint2 = null;
            try {
                fingerPrint2 = dp.getSample(null, "Scan your right index finger\n");
                dp.process(print.getRightIndex(), fingerPrint2);
            } catch (InterruptedException ex) {
            }
            if (fingerPrint2 == null) {
                try {
                    throw new Exception();
                } catch (Exception ex) {
                }
            }

            Login login = new Login();
            Map response = new HashMap<>();
            try {
                response = login.validateFingerPrint(print.getCardNumber().getText(), fingerPrint1, fingerPrint2, print.getStatus());
            } catch (SQLException ex) {
            }
            if (((String) response.get("Error")).equals("false")) {
                Border border = BorderFactory.createLineBorder(Color.GREEN, 2);
                print.getRightThumb().setBorder(border);
                print.getRightIndex().setBorder(border);
                Home fg = new Home();
                fg.getCardNumber().setText(((String) response.get("Card")));
                fg.getAccountNumber().setText(((String) response.get("Account")));
                if (((String) response.get("Alternate")).equals("true")) {
                    fg.getAddAltUser().setVisible(false);
                    fg.setAlternateUser(true);
                }
                print.setVisible(false);
                fg.setVisible(true);
            } else if (((String) response.get("Error")).equals("true")) {
                print.getRightThumb().setIcon(null);
                Border border = BorderFactory.createLineBorder(Color.red, 2);
                print.getRightThumb().setBorder(border);
                print.getRightIndex().setIcon(null);
                print.getRightIndex().setBorder(border);
                print.getError().setText((String) response.get("Message"));
                print.getError().setForeground(Color.red);
                SwingWorker worker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        FingerprintVerify fingerprint = new FingerprintVerify();
                        return null;
                    }
                };
                worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
                    if ("DONE".equals(evt.getNewValue().toString())) {
                    }
                });
                worker.execute();
            }
        }
    }

    class Back implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            dp.getFingerCapture().stopCapture();
            CardNumber card = new CardNumber();
            print.setVisible(false);
            card.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

}
