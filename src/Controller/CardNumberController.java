/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Login;
import View.Pin;
import View.RegisterUser;
import View.CardNumber;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 *
 * @author the Code
 */
public class CardNumberController {

    public final CardNumber home;
    public final Login model;

    public CardNumberController(CardNumber home, Login model) {
        this.home = home;
        this.model = model;
        
        this.home.setTitle("Card Number Verification");

        this.home.getRegistrationLink().addMouseListener(new RegistrationLink());
        this.home.getATMUsername().addKeyListener(new CheckValues());

        this.home.getATMUsername().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.home.getATMUsername().getActionMap().put("pressed", new ValidateCardEnter());
        this.home.getATMLoginBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.home.getATMLoginBtn().getActionMap().put("pressed", new ValidateCardEnter());

        this.home.getATMLoginBtn().addActionListener(new ValidateCard());
    }

    public boolean Validate(JTextField cardNo, JLabel warning) {
        String regex = "\\d+";
        if (cardNo.getText().equals("")) {
            warning.setText("Please fill in card number");
            warning.setForeground(Color.red);
            cardNo.requestFocusInWindow();
            return true;
        } else if (!cardNo.getText().matches(regex)) {
            warning.setText("Invalid Card Number");
            warning.setForeground(Color.red);
            cardNo.setText("");
            cardNo.requestFocusInWindow();
            return true;
        }
        return false;
    }

    class ValidateCard implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!Validate(home.getATMUsername(), home.getWarning())) {
                Map response = new HashMap<>();
                try {
                    response = model.validateCard(home.getATMUsername().getText());
                } catch (SQLException ex) {
                }
                if (((String) response.get("Error")).equals("false")) {
                    Pin h = new Pin();
                    h.setCardNo((String) response.get("Card"));
                    h.setNotice("Welcome " + (String) response.get("Name"));
                    home.setVisible(false);
                    h.setVisible(true);
                } else if (((String) response.get("Error")).equals("true")) {
                    home.getWarning().setText((String) response.get("Message"));
                    home.getATMUsername().requestFocusInWindow();
                }
            }
        }

    }

    class ValidateCardEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!Validate(home.getATMUsername(), home.getWarning())) {
                Map response = new HashMap<>();
                try {
                    response = model.validateCard(home.getATMUsername().getText());
                } catch (SQLException ex) {
                }
                if (((String) response.get("Error")).equals("false")) {
                    Pin h = new Pin();
                    h.setCardNo((String) response.get("Card"));
                    h.setNotice("Welcome " + (String) response.get("Name"));
                    home.setVisible(false);
                    h.setVisible(true);
                } else if (((String) response.get("Error")).equals("true")) {
                    home.getWarning().setText((String) response.get("Message"));
                    home.getATMUsername().requestFocusInWindow();
                }
            }
        }

    }

    class RegistrationLink implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            RegisterUser reg = new RegisterUser();
            home.setVisible(false);
            reg.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            home.getRegistrationLink().setForeground(Color.BLUE);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            home.getRegistrationLink().setForeground(Color.BLACK);
        }

    }

    //check To avoid illegal characters in pin
    class CheckValues implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            String pass = ((JTextField) e.getSource()).getText();
            if (pass.length() < 16) {
                if ((!(Character.isDigit(c))) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)) {
                    Toolkit.getDefaultToolkit().beep();
                    e.consume();
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

}
