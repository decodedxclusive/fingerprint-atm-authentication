/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.DPFPCapturePriority;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.capture.event.DPFPSensorEvent;
import com.digitalpersona.onetouch.capture.event.DPFPSensorListener;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.readers.DPFPReadersCollection;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import java.awt.Image;
import java.util.EnumMap;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class FingerprintVerification {

    //static EnumMap<DPFPFingerIndex, DPFPTemplate> templates;
    static EnumMap<DPFPFingerIndex, DPFPTemplate> templates = new EnumMap<DPFPFingerIndex, DPFPTemplate>(DPFPFingerIndex.class);
    public JTextArea status;

    public FingerprintVerification() {
    }

    public FingerprintVerification(JTextArea stat) {
        this.status = stat;
    }

    public void listReaders() {
        DPFPReadersCollection readers = DPFPGlobal.getReadersFactory().getReaders();
        if (readers == null || readers.isEmpty()) {
            status.append("There are no readers available.\n");
            return;
        }
        status.append("Available readers:\n");
        readers.forEach((readerDescription) -> {
            status.append(readerDescription.getSerialNumber() + "\n");
        });
    }

    public static final EnumMap<DPFPFingerIndex, String> fingerNames;

    static {
        fingerNames = new EnumMap<>(DPFPFingerIndex.class);
        fingerNames.put(DPFPFingerIndex.LEFT_PINKY, "left pinky");
        fingerNames.put(DPFPFingerIndex.LEFT_RING, "left ring");
        fingerNames.put(DPFPFingerIndex.LEFT_MIDDLE, "left middle");
        fingerNames.put(DPFPFingerIndex.LEFT_INDEX, "left index");
        fingerNames.put(DPFPFingerIndex.LEFT_THUMB, "left thumb");
        fingerNames.put(DPFPFingerIndex.RIGHT_PINKY, "right pinky");
        fingerNames.put(DPFPFingerIndex.RIGHT_RING, "right ring");
        fingerNames.put(DPFPFingerIndex.RIGHT_MIDDLE, "right middle");
        fingerNames.put(DPFPFingerIndex.RIGHT_INDEX, "right index");
        fingerNames.put(DPFPFingerIndex.RIGHT_THUMB, "right thumb");
    }

    public DPFPTemplate getTemplate(String activeReader, int nFinger, JLabel picture) {
        status.append("Performing fingerprint enrollment...\n");

        DPFPTemplate template = null;

        try {
            DPFPFingerIndex finger = DPFPFingerIndex.values()[nFinger];
            DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
            DPFPEnrollment enrollment = DPFPGlobal.getEnrollmentFactory().createEnrollment();
            while (enrollment.getFeaturesNeeded() > 0) {
                DPFPSample sample = getSample(activeReader,
                        String.format("Scan your %s finger (%d remaining)\n", fingerName(finger), enrollment.getFeaturesNeeded()));
                if (sample == null) {
                    continue;
                }
                process(picture, sample);
                DPFPFeatureSet featureSet;
                try {
                    featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
                } catch (DPFPImageQualityException e) {
                    status.append("Bad image quality: " + e.getCaptureFeedback().toString() + ". Try again.\n");
                    continue;
                }

                enrollment.addFeatures(featureSet);
            }
            template = enrollment.getTemplate();
            status.append("The " + fingerprintName(finger) + " was enrolled.\n");
        } catch (DPFPImageQualityException e) {
            status.append("Failed to enroll the finger.\n");
            getTemplate(activeReader, nFinger, picture);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return template;
    }

    public boolean verify(String activeReader, DPFPSample sample, DPFPTemplate template) {

        try {

            DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
            DPFPFeatureSet featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

            DPFPVerification matcher = DPFPGlobal.getVerificationFactory().createVerification();
            matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

            for (DPFPFingerIndex finger : DPFPFingerIndex.values()) {
                //DPFPTemplate template = user.getTemplate(finger); 
                if (template != null) {
                    DPFPVerificationResult result = matcher.verify(featureSet, template);
                    if (result.isVerified()) {
                        //status.append("Matching finger: " + fingerName(finger) + ", FAR achieved: " + (double) result.getFalseAcceptRate() / DPFPVerification.PROBABILITY_ONE + ".\n");
                        return result.isVerified();
                    }
                }
            }
        } catch (DPFPImageQualityException e) {
            status.append("Failed to perform verification.\n");
        }

        return false;
    }
    
    DPFPCapture capture;
    
    public DPFPCapture getFingerCapture(){
        return capture;
    }

    public DPFPSample getSample(String activeReader, String prompt)
            throws InterruptedException {
        final LinkedBlockingQueue<DPFPSample> samples = new LinkedBlockingQueue<>();
        capture = DPFPGlobal.getCaptureFactory().createCapture();
        capture.setReaderSerialNumber(activeReader);
        capture.setPriority(DPFPCapturePriority.CAPTURE_PRIORITY_LOW);
        capture.addDataListener((DPFPDataEvent e) -> {
            if (e != null && e.getSample() != null) {

                try {
                    samples.put(e.getSample());
                } catch (InterruptedException e1) {
                }
            }
        });
        capture.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            int lastStatus = DPFPReaderStatusEvent.READER_CONNECTED;

            @Override
            public void readerConnected(DPFPReaderStatusEvent e) {
                if (lastStatus != e.getReaderStatus()) {
                    status.append("Reader is connected\n");
                }
                lastStatus = e.getReaderStatus();
            }

            @Override
            public void readerDisconnected(DPFPReaderStatusEvent e) {
                if (lastStatus != e.getReaderStatus()) {
                    status.append("Reader is disconnected\n");
                }
                lastStatus = e.getReaderStatus();
            }

        });
        capture.addSensorListener(new DPFPSensorListener() {
            @Override
            public void fingerTouched(DPFPSensorEvent dpfpse) {
                //status.append("Fingerprint touched\n");
            }

            @Override
            public void fingerGone(DPFPSensorEvent dpfpse) {
                //status.append("Fingerprint removed\n");
            }

            @Override
            public void imageAcquired(DPFPSensorEvent dpfpse) {
                status.append("Fingerprint acquired\n");
            }
        });
        try {
            capture.startCapture();
            status.append(prompt);
            return samples.take();
        } catch (RuntimeException e) {
            status.append("Failed to start capture. Check that reader is not used by another application.\n");
            throw e;
        } finally {
            capture.stopCapture();
        }
    }

    public String fingerName(DPFPFingerIndex finger) {
        return fingerNames.get(finger);
    }

    public String fingerprintName(DPFPFingerIndex finger) {
        return fingerNames.get(finger) + " fingerprint";
    }

    public void process(JLabel picture, DPFPSample sample) {
        drawPicture(picture, convertSampleToBitmap(sample));
    }

    private void drawPicture(JLabel picture, Image image) {
        picture.setIcon(new ImageIcon(image.getScaledInstance(180, 240, Image.SCALE_DEFAULT)));
    }

    private Image convertSampleToBitmap(DPFPSample sample) {
        return DPFPGlobal.getSampleConversionFactory().createImage(sample);
    }
}
