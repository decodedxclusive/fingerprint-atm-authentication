/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Account;
import Model.FingerprintVerification;
import POJO.AccountHolder;
import View.RegisterAlternateUser;
import View.Home;
import com.digitalpersona.onetouch.DPFPTemplate;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import javax.swing.text.DefaultCaret;
import static javax.swing.text.DefaultCaret.ALWAYS_UPDATE;

/**
 *
 * @author the Code
 */
public class RegisterAlternateController {

    public final RegisterAlternateUser reg;
    public final Account model;
    FingerprintVerification dp;

    byte[] fingerPrint1, fingerPrint2;

    public RegisterAlternateController(RegisterAlternateUser reg, Account model) {
        this.reg = reg;
        this.model = model;
        
        this.reg.setTitle("Add Alternate User");

        this.reg.getBackHome().addMouseListener(new Back());
        this.reg.getFirstName().addKeyListener(new CheckNames());
        this.reg.getLastName().addKeyListener(new CheckNames());

        DefaultCaret caret = (DefaultCaret) this.reg.getFingerprintStatus().getCaret();
        caret.setUpdatePolicy(ALWAYS_UPDATE);
        this.reg.getFingerprintStatus().setCaret(caret);

        this.reg.getRegBtn().setEnabled(false);

        this.reg.getFirstName().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.reg.getFirstName().getActionMap().put("pressed", new FirstNameEnter());
        this.reg.getLastName().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.reg.getLastName().getActionMap().put("pressed", new LastNameEnter());

        this.reg.getRegBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.reg.getRegBtn().getActionMap().put("pressed", new RegisterEnter());

        this.reg.getRegBtn().addActionListener(new Register());

        SwingWorker worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                Fingerprint fingerprint = new Fingerprint();
                return null;
            }
        };
        worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if ("DONE".equals(evt.getNewValue().toString())) {
            }
        });
        worker.execute();
    }

    public boolean Validate(AccountHolder account, JLabel warning) {
        if (account.getFirstName().equals("")) {
            warning.setText("Please fill in first name");
            warning.setForeground(Color.red);
            return true;
        } else if (account.getLastName().equals("")) {
            warning.setText("Please fill in last name");
            warning.setForeground(Color.red);
            return true;
        }
        return false;
    }

    public void ClearFields(JTextField fname, JTextField lname) {
        fname.setText("");
        lname.setText("");
    }

    class Register implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            AccountHolder user = new AccountHolder(reg.getFirstName().getText(), reg.getLastName().getText());
            if (!Validate(user, reg.getWarning())) {
                reg.getWarning().setText("");
                user.setAccountNumber(reg.getAccountNumber().getText());
                user.setCardNumber(reg.getCardNumber().getText());
                Map added = new HashMap<>();
                try {
                    added = model.AlternateUser(user, fingerPrint1, fingerPrint2);
                } catch (SQLException ex) {
                }
                if (((String) added.get("Error")).equals("false")) {
                    reg.getWarning().setText("Account Successfully added");
                    reg.getWarning().setForeground(Color.green);
                    reg.setPin((String) added.get("Pin"));
                    ClearFields(reg.getFirstName(), reg.getLastName());
                    reg.getFingerprintStatus().setText("");
                    SwingWorker worker = new SwingWorker<Void, Void>() {
                        @Override
                        protected Void doInBackground() throws Exception {
                            Fingerprint fingerprint = new Fingerprint();
                            return null;
                        }
                    };
                    worker.addPropertyChangeListener((PropertyChangeEvent evt) -> {
                        if ("DONE".equals(evt.getNewValue().toString())) {
                        }
                    });
                    worker.execute();
                } else if (((String) added.get("Error")).equals("true")) {
                    reg.getWarning().setText("Error adding account");
                    reg.getWarning().setForeground(Color.red);
                }
            }
        }
    }

    class RegisterEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {

        }

    }

    //check To avoid illegal characters in pin
    class CheckNames implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if ((!(Character.isAlphabetic(c))) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)) {
                Toolkit.getDefaultToolkit().beep();
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    class FirstNameEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            reg.getLastName().requestFocusInWindow();
        }

    }

    class LastNameEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            reg.getRegBtn().requestFocusInWindow();
        }

    }

    class Fingerprint {

        public Fingerprint() {

            dp = new FingerprintVerification(reg.getFingerprintStatus());
            dp.listReaders();

            DPFPTemplate temp = dp.getTemplate(null, 5, reg.getRightThumb());
            fingerPrint1 = temp.serialize();

            DPFPTemplate temp2 = dp.getTemplate(null, 6, reg.getRightIndex());
            fingerPrint2 = temp2.serialize();

            if (fingerPrint1.length > 0 && fingerPrint2.length > 0) {
                reg.getRegBtn().setEnabled(true);
                reg.getRegBtn().requestFocusInWindow();
            }
        }
    }

    class Back implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            dp.getFingerCapture().stopCapture();
            Home home = new Home();
            home.getAccountNumber().setText(reg.getAccountNumber().getText());
            home.getCardNumber().setText(reg.getCardNumber().getText());
            reg.setVisible(false);
            home.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

}
