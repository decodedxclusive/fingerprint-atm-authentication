/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Account;
import POJO.ResetUserPin;
import View.Home;
import View.ResetPin;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;

/**
 *
 * @author the Code
 */
public class ResetPinController {

    private final ResetPin resetPin;
    private final Account accountModel;

    public ResetPinController(ResetPin resetPin, Account accountModel) {
        this.resetPin = resetPin;
        this.accountModel = accountModel;

        this.resetPin.setTitle("Reset Password");

        this.resetPin.getOldPin().addKeyListener(new CheckValues());
        this.resetPin.getNewPin().addKeyListener(new CheckValues());

        this.resetPin.getOldPin().addActionListener(new OldPin());
        this.resetPin.getNewPin().addActionListener(new NewPin());

        this.resetPin.getConfirmNewPin().addKeyListener(new CheckValues());

        this.resetPin.getConfirmNewPin().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.resetPin.getConfirmNewPin().getActionMap().put("pressed", new ResetEnter());
        this.resetPin.getResetBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        this.resetPin.getResetBtn().getActionMap().put("pressed", new ResetEnter());

        this.resetPin.getResetBtn().addActionListener(new Reset());

        this.resetPin.getBackbtn().addMouseListener(new Back());
    }

    class Reset implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (resetPin.getOldPin().getPassword().equals("") || resetPin.getNewPin().getPassword().equals("") || resetPin.getConfirmNewPin().getPassword().equals("")) {
                resetPin.getWarning().setForeground(Color.red);
                resetPin.getWarning().setText("Please complete form");
            } else {
                ResetUserPin resetUser = new ResetUserPin(resetPin.getCardNumber().getText(), resetPin.isAlternateUser());
                resetUser.setOldPin(resetPin.getOldPin().getPassword());
                resetUser.setNewPin(resetPin.getNewPin().getPassword());
                resetUser.setConfirmPin(resetPin.getConfirmNewPin().getPassword());

                Map<String, String> status = new HashMap<>();
                try {
                    if (!resetUser.isAlternateUser()) {
                        status = accountModel.UpdateUserPassword(resetUser);
                    } else {
                        status = accountModel.UpdateAlternateUserPassword(resetUser);
                    }
                } catch (SQLException ex) {
                }

                if (status.get("Error").equals("false")) {
                    resetPin.getOldPin().setText("");
                    resetPin.getNewPin().setText("");
                    resetPin.getConfirmNewPin().setText("");
                    resetPin.getWarning().setForeground(Color.green);
                    resetPin.getWarning().setText("Pin reset successful");
                } else {
                    resetPin.getWarning().setForeground(Color.red);
                    resetPin.getWarning().setText(status.get("message"));
                }
            }
        }

    }

    class ResetEnter extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            ResetUserPin resetUser = new ResetUserPin(resetPin.getCardNumber().getText(), resetPin.isAlternateUser());
            resetUser.setOldPin(resetPin.getOldPin().getPassword());
            resetUser.setNewPin(resetPin.getNewPin().getPassword());
            resetUser.setConfirmPin(resetPin.getConfirmNewPin().getPassword());

            Map<String, String> status = new HashMap<>();
            try {
                if (!resetUser.isAlternateUser()) {
                    status = accountModel.UpdateUserPassword(resetUser);
                } else {
                    status = accountModel.UpdateAlternateUserPassword(resetUser);
                }
            } catch (SQLException ex) {
            }

            if (status.get("Error").equals("false")) {
                resetPin.getOldPin().setText("");
                resetPin.getNewPin().setText("");
                resetPin.getConfirmNewPin().setText("");
                resetPin.getWarning().setForeground(Color.green);
                resetPin.getWarning().setText("Pin reset successful");
            } else {
                resetPin.getWarning().setForeground(Color.red);
                resetPin.getWarning().setText(status.get("message"));
            }
        }

    }

    class OldPin implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            resetPin.getNewPin().requestFocusInWindow();
        }

    }

    class NewPin implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            resetPin.getConfirmNewPin().requestFocusInWindow();
        }

    }

    //check To avoid illegal characters in pin
    class CheckValues implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            char[] pass = ((JPasswordField) e.getSource()).getPassword();
            if (pass.length < 4) {
                if ((!(Character.isDigit(c))) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)) {
                    Toolkit.getDefaultToolkit().beep();
                    e.consume();
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    class Back implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            Home home = new Home();
            home.getCardNumber().setText(resetPin.getCardNumber().getText());
            home.getAccountNumber().setText(resetPin.getAccountNumber());
            if (resetPin.isAlternateUser()) {
                home.getAddAltUser().setVisible(false);
                home.setAlternateUser(true);
            }
            resetPin.setVisible(false);
            home.setVisible(true);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

}
